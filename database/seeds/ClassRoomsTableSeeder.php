<?php

use Illuminate\Database\Seeder;
use App\ClassRoom;

class ClassRoomsTableSeeder extends Seeder
{
    /**
     * Initial data.
     *
     * @var array
     */
    protected $data = [
        // PL1
        ['id' => 1, 'grade' => 1, 'name' => 'Kelas 1 - PL1'],
        ['id' => 2, 'grade' => 2, 'name' => 'Kelas 2 - PL1'],
        ['id' => 3, 'grade' => 3, 'name' => 'Kelas 3 - PL1'],
        ['id' => 4, 'grade' => 4, 'name' => 'Kelas 4 - PL1'],
        ['id' => 5, 'grade' => 5, 'name' => 'Kelas 5 - PL1'],
        ['id' => 6, 'grade' => 6, 'name' => 'Kelas 6 - PL1'],

        // PL2
        ['id' => 7, 'grade' => 1, 'name' => 'Kelas 1 - PL2'],
        ['id' => 8, 'grade' => 2, 'name' => 'Kelas 2 - PL2'],
        ['id' => 9, 'grade' => 3, 'name' => 'Kelas 3 - PL2'],
        ['id' => 10, 'grade' => 4, 'name' => 'Kelas 4 - PL2'],
        ['id' => 11, 'grade' => 5, 'name' => 'Kelas 5 - PL2'],
        ['id' => 12, 'grade' => 6, 'name' => 'Kelas 6 - PL2'],

        // PL3
        ['id' => 13, 'grade' => 1, 'name' => 'Kelas 1 - PL3'],
        ['id' => 14, 'grade' => 2, 'name' => 'Kelas 2 - PL3'],
        ['id' => 15, 'grade' => 3, 'name' => 'Kelas 3 - PL3'],
        ['id' => 16, 'grade' => 4, 'name' => 'Kelas 4 - PL3'],
        ['id' => 17, 'grade' => 5, 'name' => 'Kelas 5 - PL3'],
        ['id' => 18, 'grade' => 6, 'name' => 'Kelas 6 - PL3'],

        // PL4
        ['id' => 19, 'grade' => 1, 'name' => 'Kelas 1 - PL4'],
        ['id' => 20, 'grade' => 2, 'name' => 'Kelas 2 - PL4'],
        ['id' => 21, 'grade' => 3, 'name' => 'Kelas 3 - PL4'],
        ['id' => 22, 'grade' => 4, 'name' => 'Kelas 4 - PL4'],
        ['id' => 23, 'grade' => 5, 'name' => 'Kelas 5 - PL4'],
        ['id' => 24, 'grade' => 6, 'name' => 'Kelas 6 - PL4'],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect($this->data)->each(function ($item) {
            $class_room = ClassRoom::find($item['id']);

            if (is_null($class_room)) {
                ClassRoom::create($item);
            } else {
                unset($item['id']);
                $class_room->update($item);
            }
        });
    }
}
