<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Initial data.
     *
     * @var array
     */
    protected $data = [
        [
            'id' => 1,
            'name' => 'admin',
            'full_name' => 'Administrator',
            'password' => 'admin',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect($this->data)->each(function ($data) {
            $data['password'] = bcrypt($data['password']);

            User::create($data);
        });
    }
}
