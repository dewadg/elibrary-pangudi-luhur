<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Initial data.
     *
     * @var array
     */
    protected $data = [
        [
            'id' => 1,
            'name' => 'Novel',
        ],
        [
            'id' => 2,
            'name' => 'Komik',
        ],
        [
            'id' => 3,
            'name' => 'Ensiklopedia',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect($this->data)->each(function ($data) {
            Category::create($data);
        });
    }
}
