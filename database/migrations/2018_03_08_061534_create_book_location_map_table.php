<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookLocationMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_location_map', function (Blueprint $table) {
            $table->unsignedInteger('book_id');
            $table->foreign('book_id')
                ->references('id')->on('books');
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')
                ->references('id')->on('locations');
            $table->unsignedInteger('qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_location_map');
    }
}
