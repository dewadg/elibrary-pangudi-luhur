<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isbn', 13)
                ->unique()
                ->nullable();
            $table->string('title');
            $table->date('date_of_publication')->nullable();
            $table->unsignedInteger('qty');
            $table->unsignedInteger('available');
            $table->unsignedInteger('in_rent')->default(0);
            $table->unsignedInteger('broken')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
