<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorBookMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('author_book_map', function (Blueprint $table) {
            $table->unsignedInteger('book_id');
            $table->foreign('book_id')
                ->references('id')->on('books');
            $table->unsignedInteger('author_id');
            $table->foreign('author_id')
                ->references('id')->on('authors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('author_book_map');
    }
}
