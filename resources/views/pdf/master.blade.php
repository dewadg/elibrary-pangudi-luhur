<!DOCTYPE html>
<html>
<head>
  <title>@yield('title')</title>
  <style>
    body {
      font-family: "Arial", sans-serif;
      font-size: 14px;
    }

    .clear {
      width: 100%;
      clear: both;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    header h1 {
      float: left;
      font-size: 20px;
      line-height: 20px;
    }

    header .date {
      display: block;
      float: right;
      padding-top: 4px;
    }

    .report {
      width: 100%;
      border-collapse: collapse;
    }

    .report thead tr th,
    .report tbody tr th,
    .report tbody tr td {
      padding: 5px;
    }
  </style>
  @yield('style')
</head>
<body>
  <header>
    <h1>@yield('title')</h1>
    <span class="date">{{ date('j F Y H:i') }}</span>
    <div class="clear"></div>
  </header>
  @yield('content')
</body>
</html>
