<!DOCTYPE html>
<html>
<head>
  <title>Print Out Kartu Keanggotaan Siswa</title>
  <style>
    @page {
      margin: 0.5cm;
    }

    body {
      margin: 0;
      font-family: "Arial", sans-serif;
    }

    .card {
      width: 8.8cm;
      height: 4.8cm;
      border: 1px solid #000;
      padding: 0.2cm;
      font-size: 10px;
    }

    .card h2 {
      margin: 0 0 0.25cm 0;
      font-size: 12px;
      text-align: center;
      text-transform: uppercase;
    }

    .card .img-slot {
      padding-right: 20px;
    }

    .card .img {
      width: 2cm;
      height: 3cm;
      border: 1px solid #000;
    }
  </style>
</head>
<body>
  <table>
    <tbody>
      @foreach($students->chunk(2) as $row)
        <tr>
          @foreach($row as $student)
            <td class="card">
              <h2>Perpustakaan SD Pangudi Luhur Yogyakarta</h2>
              <table>
                <tbody>
                  <tr>
                    <td class="img-slot" rowspan="4">
                      <div class="img"> </div>
                    </td>
                    <td>NISN: {{ $student->id }}</td>
                  </tr>
                  <tr>
                    <td>Nama: {{ $student->name }}</td>
                  </tr>
                  <tr>
                    <td>Kelas: {{ $student->classRoom->name }}</td>
                  </tr>
                  <tr>
                    <td>
                      <center><img src="{{ $student->barcode_string }}"></center>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
            <td style="width: 0.5cm"> </td>
          @endforeach
        </tr>
        <tr>
          <td colspan="2" style="height: 0.5cm"> </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</body>
</html>
