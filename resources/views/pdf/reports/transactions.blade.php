@extends('pdf.master')
@section('title', $title)

@section('content')
<table class="report" border="1">
  <thead>
    <tr>
      <th width="15%">NISN</th>
      <th>Nama</th>
      <th width="10%" class="text-center">Total</th>
    </tr>
  </thead>
  <tbody>
    @foreach($students as $student)
      <tr>
        <td>{{ $student['id'] }}</td>
        <td>{{ $student['name'] }}</td>
        <td class="text-center">{{ $student['total'] }}</td>
      </tr>
    @endforeach
    <tr>
      <th colspan="2" class="text-right">TOTAL</th>
      <th class="text-center">{{ $total }}</th>
    </tr>
  </tbody>
</table>
@endsection
