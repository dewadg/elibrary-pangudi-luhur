@extends('pdf.master')
@section('title', $title)

@section('content')
<table class="report" border="1">
  <thead>
    <tr>
      <th width="12.5%" class="text-center">Tgl</th>
      @foreach($class_rooms as $class_room)
        <th width="12.5%" class="text-center">{{ $class_room->name }}</th>
      @endforeach
      <th width="12.5%" class="text-center">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $date => $row)
      <tr>
        <td class="text-center">{{ $date }}</td>
        @foreach($row as $class_room_data)
          <td class="text-center">{{ $class_room_data['count'] == 0 ? '' : $class_room_data['count'] }}</td>
        @endforeach
      </tr>
    @endforeach
  </tbody>
</table>
@endsection
