@extends('pdf.master')
@section('title', 'Laporan Buku Rusak')

@section('content')
<table class="report" border="1">
  <thead>
    <tr>
      <th width="15%">ISBN</th>
      <th width="40%">Judul</th>
      <th>Kategori</th>
      <th width="10%" class="text-center">Qty</th>
    </tr>
  </thead>
  <tbody>
    @foreach($books as $book)
      <tr>
        <td>{{ $book->isbn }}</td>
        <td>{{ $book->title }}</td>
        <td>{{ $book->categories }}</td>
        <td class="text-center">{{ $book->broken }}</td>
      </tr>
    @endforeach
    <tr>
      <th colspan="3" class="text-right">TOTAL</th>
      <th class="text-center">{{ $total }}</th>
    </tr>
  </tbody>
</table>
@endsection
