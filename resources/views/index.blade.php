<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
  <title>eLibrary SD Pangudi Luhur</title>
</head>

<body>
  <div id="app"></div>
  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>