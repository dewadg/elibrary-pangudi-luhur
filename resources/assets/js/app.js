import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuelidate from 'vuelidate';
import moment from 'moment';
import App from './components/App';
import router from './routes';
import store from './stores';
import Http from './services/Http';

Vue.use(Vuetify);
Vue.use(Vuelidate);

Http.init();
moment.locale('id');

new Vue({
  router,
  store,

  render(h) {
    return h(App);
  },
})
  .$mount('#app');
