import Http from '../services/Http';

const state = {
  data: [],
};

const mutations = {
  setSource(state, source) {
    state.data = source;
  },
};

const actions = {
  fetchAll(context) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        context.commit('setSource', res.data);
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get('transactions', successCallback, errorCallback);
    });
  },

  fetch(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        resolve(res.data);
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get(`transactions/${id}`, successCallback, errorCallback);
    });
  },

  rent(context, payload) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.post('transactions/rent', payload, successCallback, errorCallback);
    });
  },

  returnTrans(context, payload) {
    return new Promise((resolve, reject) => {
      const { id, books } = payload;
      const data = {
        books,
      };

      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.post(`transactions/${id}/return`, data, successCallback, errorCallback);
    });
  },

  delete(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.delete(`transactions/${id}`, successCallback, errorCallback);
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
