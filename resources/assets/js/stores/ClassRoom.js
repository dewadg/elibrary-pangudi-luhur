import Http from '../services/Http';

const state = {
  data: [],
};

const mutations = {
  setSource(state, source) {
    state.data = source;
  },
};

const actions = {
  fetchAll(context) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        context.commit('setSource', res.data);
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get('class-rooms', successCallback, errorCallback);
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
