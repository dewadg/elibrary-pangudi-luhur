import Http from '../services/Http';

const state = {
  data: [],
};

const mutations = {
  setSource(state, source) {
    state.data = source;
  },
};

const actions = {
  fetchAll(context) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        context.commit('setSource', res.data);
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get('books', successCallback, errorCallback);
    });
  },

  fetch(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        resolve(res.data);
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get(`books/${id}`, successCallback, errorCallback);
    });
  },

  store(context, payload) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        if (res.status === 201) {
          resolve();
        }
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.post('books', payload, successCallback, errorCallback);
    });
  },

  update(context, payload) {
    return new Promise((resolve, reject) => {
      const {
        id,
        isbn,
        title,
        date_of_publication,
        qty,
        available,
        in_rent,
        broken,
        publisher_id,
        categories,
        authors,
      } = payload;

      const data = {
        isbn,
        title,
        date_of_publication,
        qty,
        available,
        in_rent,
        broken,
        publisher_id,
        categories,
        authors,
      };

      const successCallback = (res) => {
        resolve(res.data);
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.patch(`books/${id}`, data, successCallback, errorCallback);
    });
  },

  delete(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.delete(`books/${id}`, successCallback, errorCallback);
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
