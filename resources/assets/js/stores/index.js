import Vue from 'vue';
import Vuex from 'vuex';
import LoggedUser from './LoggedUser';
import ClassRoom from './ClassRoom';
import Student from './Student';
import Category from './Category';
import Book from './Book';
import Publisher from './Publisher';
import Author from './Author';
import Transaction from './Transaction';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    LoggedUser,
    ClassRoom,
    Student,
    Category,
    Book,
    Publisher,
    Author,
    Transaction,
  },
});

export default store;
