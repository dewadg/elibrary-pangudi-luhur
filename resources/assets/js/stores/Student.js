import Http from '../services/Http';

const state = {
  data: [],
};

const mutations = {
  setSource(state, source) {
    state.data = source;
  },
};

const actions = {
  fetchAll(context) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        context.commit('setSource', res.data);
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get('students', successCallback, errorCallback);
    });
  },

  fetch(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        resolve(res.data);
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get(`students/${id}`, successCallback, errorCallback);
    });
  },

  store(context, payload) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        if (res.status === 201) {
          resolve();
        }
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.post('students', payload, successCallback, errorCallback);
    });
  },

  update(context, payload) {
    return new Promise((resolve, reject) => {
      const {
        id,
        student_number,
        name,
        date_of_birth,
        gender,
        class_room_id,
      } = payload;

      const data = {
        student_number,
        name,
        date_of_birth,
        gender,
        class_room_id,
      };

      const successCallback = (res) => {
        resolve(res.data);
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.patch(`students/${id}`, data, successCallback, errorCallback);
    });
  },

  delete(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.delete(`students/${id}`, successCallback, errorCallback);
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
