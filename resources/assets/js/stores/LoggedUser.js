export default {
  namespaced: true,

  state: {
    data: {
      name: '',
      full_name: '',
    },
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
  },

  getters: {
    fullName: state => state.data.full_name,
  },
};
