import axios from 'axios';
import router from '../routes';

function createInstance(jwt) {
  const config = {
    baseURL: '/api',
  };

  if (jwt !== '' && jwt !== null) {
    config.headers = {
      Authorization: `Bearer ${jwt}`,
    };
  }

  return axios.create(config);
}

export default {
  axios: null,

  init(jwt = null) {
    this.axios = createInstance(jwt);
  },

  setJwt(jwt) {
    this.init(jwt);
  },

  interceptError(err) {
    if (err.status === 400) {
      router.push({ name: 'login' });
    }
  },

  post(url, data, successCallback, errorCallback) {
    return this.axios.request({
      url,
      data,
      method: 'post',
    })
      .then(successCallback)
      .catch((err) => {
        this.interceptError(err);

        errorCallback(err);
      });
  },

  get(url, successCallback, errorCallback) {
    return this.axios.request({
      url,
      method: 'get',
    })
      .then(successCallback)
      .catch((err) => {
        this.interceptError(err);

        errorCallback(err);
      });
  },

  patch(url, data, successCallback, errorCallback) {
    return this.axios.request({
      url,
      data,
      method: 'patch',
    })
      .then(successCallback)
      .catch((err) => {
        this.interceptError(err);

        errorCallback(err);
      });
  },

  delete(url, successCallback, errorCallback) {
    return this.axios.request({
      url,
      method: 'delete',
    })
      .then(successCallback)
      .catch((err) => {
        this.interceptError(err);

        errorCallback(err);
      });
  },
};
