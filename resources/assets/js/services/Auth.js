import jwt from 'jsonwebtoken';
import Cookies from 'js-cookie';
import Http from './Http';
import store from '../stores';

export default {
  authenticate(name, password) {
    return new Promise((resolve, reject) => {
      const payload = {
        name,
        password,
      };

      const successCallback = async (res) => {
        const token = res.data;

        this.storeJwt(token);
        Http.setJwt(token);

        const user = await this.whoami();
        store.commit('LoggedUser/setSource', user);

        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.post('auth', payload, successCallback, errorCallback);
    });
  },

  whoami() {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        resolve(res.data);
      };

      const errorCallback = (err) => {
        reject(err);
      };

      Http.get('whoami', successCallback, errorCallback);
    });
  },

  storeJwt(token) {
    Cookies.set('jwt', token);
  },

  getJwt() {
    return Cookies.get('jwt');
  },

  decodeJwt(token) {
    const decodedToken = jwt.decode(token, { complete: true });

    return decodedToken.payload;
  },

  clearJwt() {
    Cookies.remove('jwt');
  },

  async refetchToken() {
    const token = this.getJwt();
    Http.setJwt(token);

    const user = await this.whoami();
    store.commit('LoggedUser/setSource', user);
  },
};
