import {
  required,
  minLength,
  maxLength,
} from 'vuelidate/lib/validators';
import Http from '../services/Http';

export default {
  id: {
    required,
    minLength: minLength(10),
    maxLength: maxLength(10),

    isUnique(value) {
      if (value === '' || this.edit) return true;

      return new Promise((resolve, reject) => {
        const successCallback = (res) => {
          resolve(res.data);
        };

        const errorCallback = (err) => {
          const errData = Object.assign({}, err.response);
          reject(errData);
        };

        Http.get(`students/check-id/${value}`, successCallback, errorCallback);
      });
    },
  },
  name: {
    required,
  },
  date_of_birth: {
    required,
  },
  gender: {
    required,
  },
  class_room_id: {
    required,
  },
};
