import { required } from 'vuelidate/lib/validators';

export default {
  title: { required },
  date_of_publication: { required },
  publisher_id: { required },
  categories: { required },
  authors: { required },
};
