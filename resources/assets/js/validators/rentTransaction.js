import { required } from 'vuelidate/lib/validators';

export default {
  student_id: { required },
  start: { required },
  end: { required },
  books: { required },
};
