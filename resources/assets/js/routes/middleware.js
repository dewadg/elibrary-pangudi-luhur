import Auth from '../services/Auth';

async function auth(to, from, next) {
  const jwt = Auth.getJwt();

  if (jwt === null || !jwt) {
    next({ name: 'index' });
  } else {
    await Auth.refetchToken();
    next();
  }
}

export default {
  auth,
};
