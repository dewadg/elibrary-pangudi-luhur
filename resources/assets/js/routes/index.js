import Vue from 'vue';
import VueRouter from 'vue-router';
import middleware from './middleware';

import LoginView from '../components/Login/LoginView';
import DashboardView from '../components/Dashboard/DashboardView';

import StudentIndex from '../components/Student/StudentIndex';
import StudentCreate from '../components/Student/StudentCreate';
import StudentEdit from '../components/Student/StudentEdit';
import CategoryIndex from '../components/Category/CategoryIndex';
import PublisherIndex from '../components/Publisher/PublisherIndex';
import AuthorIndex from '../components/Author/AuthorIndex';
import BookIndex from '../components/Book/BookIndex';
import TransactionIndex from '../components/Transaction/TransactionIndex';
import RentTransaction from '../components/Transaction/RentTransaction';
import BrokenBooksReport from '../components/Report/BrokenBooksReport';
import TransactionsReport from '../components/Report/TransactionsReport';

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { name: 'index', path: '/', component: LoginView },
    { name: 'dashboard', path: '/dashboard', component: DashboardView, beforeEnter: middleware.auth },
    { name: 'students.index', path: '/students', component: StudentIndex, beforeEnter: middleware.auth },
    { name: 'students.create', path: '/students/new', component: StudentCreate, beforeEnter: middleware.auth },
    { name: 'students.edit', path: '/students/:id', component: StudentEdit, beforeEnter: middleware.auth },
    { name: 'categories.index', path: '/categories', component: CategoryIndex, beforeEnter: middleware.auth },
    { name: 'publishers.index', path: '/publishers', component: PublisherIndex, beforeEnter: middleware.auth },
    { name: 'authors.index', path: '/authors', component: AuthorIndex, beforeEnter: middleware.auth },
    { name: 'books.index', path: '/books', component: BookIndex, beforeEnter: middleware.auth },
    { name: 'transactions.index', path: '/transactions', component: TransactionIndex, beforeEnter: middleware.auth },
    { name: 'transactions.rent', path: '/transactions/rent', component: RentTransaction, beforeEnter: middleware.auth },
    { name: 'reports.broken_books', path: '/report/broken-books', component: BrokenBooksReport, beforeEnter: middleware.auth },
    { name: 'reports.transactions', path: '/report/transactions', component: TransactionsReport, beforeEnter: middleware.auth },
  ],
});

export default router;
