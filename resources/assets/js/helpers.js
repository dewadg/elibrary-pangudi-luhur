import numeral from 'numeral';
import moment from 'moment';

function asCurrency(number) {
  const currency = numeral(number).format('0,0')
    .toString()
    .replace(',', '.');

  return `Rp ${currency}`;
}

function asDate(date) {
  return moment(date).format('DD/MM/YYYY');
}

export default {
  asCurrency,
  asDate,
};
