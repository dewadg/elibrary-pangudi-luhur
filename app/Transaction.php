<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    /**
     * Atrributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'student_id',
        'fine',
        'rent_at',
        'returned_at',
        'done_at',
    ];

    /**
     * Date-casted attributes.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'rent_at',
        'returned_at',
        'done_at',
    ];

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'fine' => 'double',
    ];

    /**
     * Hidden attributes.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'student_id',
    ];

    /**
     * Forms relationship to Student.
     *
     * @return Student
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Forms relationship to TransactionDetail.
     *
     * @return array
     */
    public function details()
    {
        return $this->hasMany(TransactionDetail::class);
    }
}
