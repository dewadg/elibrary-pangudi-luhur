<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ReportService;
use PDF;

class ReportController extends Controller
{
    /**
     * Stores the instance service.
     *
     * @var ReportService
     */
    protected $service;

    public function __construct()
    {
        $this->service = new ReportService;
    }

    /**
     * Returns broken books.
     *
     * @return Illuminate\Http\Response
     */
    public function brokenBooks()
    {
        $books = $this->service->brokenBooks()
            ->map(function ($item) {
                $categories = $item->categories
                    ->map(function ($item) {
                        return $item->name;
                    })
                    -all();

                $item->categories = implode(', ', $categories);

                return $item;
            });

        $pdf = PDF::loadView('pdf.reports.broken-books', compact('books'));

        return $pdf->stream();
    }
}
