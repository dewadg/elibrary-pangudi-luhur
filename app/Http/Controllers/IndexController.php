<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Displays index page
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }
}
