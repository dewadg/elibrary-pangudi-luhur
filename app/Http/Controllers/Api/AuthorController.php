<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Author;

class AuthorController extends Controller
{
    /**
     * Returns available authors.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Author::with('books')
            ->get();
    }

    /**
     * Stores new author.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            ['name' => 'required']
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        Author::create($request->all());

        return response()->json(null, 201);
    }

    /**
     * Returns author by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $author = Author::with('books')
            ->find($id);

        if (is_null($author)) {
            return response()->json(null, 404);
        }

        return $author;
    }

    /**
     * Updates author by its ID.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $author = Author::find($id);

        if (is_null($author)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            ['name' => 'required']
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $author->update($request->all());

        return response()->json();
    }

    /**
     * Deletes author by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::find($id);

        if (is_null($author)) {
            return response()->json(null, 404);
        }

        $author->delete();

        return response()->json();
    }
}
