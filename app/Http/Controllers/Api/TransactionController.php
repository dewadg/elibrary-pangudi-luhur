<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Book;
use App\Transaction;
use App\TransactionDetail;
use Carbon\Carbon;

class TransactionController extends Controller
{
    /**
     * Returns available transactions.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Transaction::with([
            'student',
            'student.classRoom',
            'details',
            'details.book',
        ])
            ->get();
    }
    
    /**
     * Performs rent transaction.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function rent(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'student_id' => 'required',
                'start' => 'required',
                'end' => 'required',
                'books' => 'required|array',
                'books.*.id' => 'required',
                'books.*.qty' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $transaction_data = $request->only(['student_id', 'start', 'end']);
            $transaction_data['user_id'] = $request->user->id;
            $transaction_data['rent_at'] = $request->get('start');
            $transaction_data['returned_at'] = $request->get('end');
            $books = collect($request->get('books'));

            DB::transaction(function () use ($transaction_data, $books) {
                $transaction = Transaction::create($transaction_data);

                $books->each(function ($item) use ($transaction) {
                    $detail = new TransactionDetail([
                        'book_id' => $item['id'],
                        'qty' => $item['qty'],
                    ]);
                    $transaction->details()->save($detail);

                    $book = Book::find($item['id']);

                    if ($book->available < $item['qty']) {
                        throw new \Exception('not_enough_books');
                    }

                    $book->decrement('available', $item['qty']);
                    $book->increment('in_rent', $item['qty']);
                });
            });

            return response()->json();
        } catch (\Exception $e) {
            if ($e->getMessage() == 'not_enough_books') {
                return response()->json($e->getMessage(), 400);
            }
            
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Returns transaction by id.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $transaction = Transaction::with([
            'student',
            'student.classRoom',
            'details',
            'details.book',
        ])
            ->find($id);

        if (is_null($transaction)) {
            return response()->json('transaction_not_found', 404);
        }

        return $transaction;
    }

    /**
     * Performs return transaction.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function return(Request $request, $id)
    {
        $transaction = Transaction::find($id);

        if (is_null($transaction)) {
            return response()->json('transaction_not_found', 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'books' => 'required|array',
                'books.*.id' => 'required',
                'books.*.qty' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $today = Carbon::now();
        $overdue_by = $today->diffInDays($transaction->returned_at);
        $user_data['done_at'] = $today;
        $returned_books = [];

        collect($request->get('books'))
            ->each(function ($item) use (&$returned_books) {
                $returned_books[$item['id']] = [
                    'qty' => $item['qty'],
                    'broken' => $item['broken'],
                ];
            });
        
        $returned_books_ids = collect($returned_books)
            ->map(function ($item, $index) {
                return $index;
            })
            ->values()
            ->all();

        // If less books were sent.
        if (count($returned_books_ids) < $transaction->details->count()) {
            return response()->json('books_not_match_previous_transaction_books_count', 422);
        }
        
        // Return is overdue, charge fine.
        if ($overdue_by > 0) {
            $user_data['fine'] = $overdue_by * 200;
        } else {
            $user_data['fine'] = 0;
        }

        try {
            DB::transaction(function () use (
                $request,
                $transaction,
                $returned_books,
                $returned_books_ids,
                $user_data
            ) {
                // Check books.
                $transaction->details
                    ->each(function ($item) use ($returned_books, $returned_books_ids) {
                        if (! in_array($item->book_id, $returned_books_ids)) {
                            throw new \Exception('no_such_book_in_this_transaction');
                        }

                        if ($returned_books[$item->book_id]['broken'] > 0) {
                            $good_in_return = $returned_books[$item->book_id]['qty'] - $returned_books[$item->book_id]['broken'];
                        } else {
                            $good_in_return = $returned_books[$item->book_id]['qty'];
                        }

                        $item->book->decrement('in_rent', $returned_books[$item->book_id]['qty']);
                        $item->book->increment('available', $good_in_return);
                        $item->book->increment('broken', $returned_books[$item->book_id]['broken']);
                    });

                $transaction->update($user_data);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes a transaction.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::find($id);

        if (is_null($transaction)) {
            return response()->json('transaction_not_found', 404);
        }

        $transaction->delete();

        return response()->json();
    }
}
