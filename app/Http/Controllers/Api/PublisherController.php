<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Publisher;

class PublisherController extends Controller
{
    /**
     * Returns available publishers.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Publisher::with('books')
            ->get();
    }

    /**
     * Stores new publisher.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            ['name' => 'required']
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        Publisher::create($request->all());

        return response()->json(null, 201);
    }

    /**
     * Returns publisher by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $publisher = Publisher::with('books')
            ->find($id);

        if (is_null($publisher)) {
            return response()->json(null, 404);
        }

        return $publisher;
    }

    /**
     * Updates publisher by its ID.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $publisher = Publisher::find($id);

        if (is_null($publisher)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            ['name' => 'required']
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $publisher->update($request->all());

        return response()->json();
    }

    /**
     * Deletes publisher by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $publisher = Publisher::find($id);

        if (is_null($publisher)) {
            return response()->json(null, 404);
        }

        $publisher->delete();

        return response()->json();
    }
}
