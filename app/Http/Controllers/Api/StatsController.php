<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\ClassRoom;
use App\Category;

class StatsController extends Controller
{
    /**
     * Returns chart data of counts, books, fine of reservations by year.
     *
     * @return Illuminate\Http\Response
     */
    public function basic()
    {
        $data = [
            '01' => ['name' => 'Januari', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '02' => ['name' => 'Februari', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '03' => ['name' => 'Maret', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '04' => ['name' => 'April', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '05' => ['name' => 'Mei', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '06' => ['name' => 'Juni', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '07' => ['name' => 'Juli', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '08' => ['name' => 'Agustus', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '09' => ['name' => 'September', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '10' => ['name' => 'Oktober', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '11' => ['name' => 'November', 'counts' => 0, 'books' => 0, 'fine' => 0],
            '12' => ['name' => 'Desember', 'counts' => 0, 'books' => 0, 'fine' => 0],
        ];

        Transaction::whereYear('created_at', date('Y'))
            ->get()
            ->groupBy(function ($item) {
                return $item->created_at->format('m');
            })
            ->each(function ($item, $key) use (&$data) {
                $data[$key]['counts'] = $item->count();

                $data[$key]['books'] = $item->sum(function ($item) {
                       $books = 0;

                       $item->each(function ($item) use (&$books) {
                            $books += $item->details->sum('qty');
                       });

                       return $books;
                    });

                $data[$key]['fine'] = $item->sum('fine');
            });

        return collect($data)->values();
    }

    /**
     * Transactions by class rooms.
     *
     * @return Illuminate\Http\Response
     */
    public function classRooms()
    {
        $class_rooms = [];

        ClassRoom::get()
            ->each(function ($item) use (&$class_rooms) {
                $class_rooms[$item->id] = [
                    'name' => $item->name,
                    'counts' => 0,
                ];
            });

        Transaction::with(['student'])
            ->whereYear('created_at', date('Y'))
            ->whereMonth('created_at', date('m'))
            ->get()
            ->groupBy(function ($item) {
                return $item->student->class_room_id;
            })
            ->each(function ($item, $key) use (&$class_rooms) {
                $class_rooms[$key]['counts'] = $item->count();
            });

        return collect($class_rooms)->values();
    }

    /**
     * Distributions of book categories.
     *
     * @return Illuminate\Http\Response
     */
    public function bookCategories()
    {
        return Category::with('books')
            ->get()
            ->map(function ($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                    'counts' => $item->books->count(),
                ];
            });
    }
}
