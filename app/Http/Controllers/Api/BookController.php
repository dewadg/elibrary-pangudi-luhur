<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Book;

class BookController extends Controller
{
    /**
     * Returns available books.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Book::with(['publisher', 'categories', 'authors'])
            ->get();
    }

    /**
     * Stores new book.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'isbn' => 'required|unique:books,isbn',
                'title' => 'required',
                'qty' => 'required',
                'available' => 'required',
                'date_of_publication' => 'required|date',
                'publisher_id' => 'required',
                'authors' => 'required|array',
                'categories' => 'required|array',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except(['categories', 'authors']);
            $categories = $request->get('categories');
            $authors = $request->get('authors');

            DB::transaction(function () use (
                $user_data,
                $categories,
                $authors
            ) {
                $book = Book::create($user_data);
                $book->categories()->sync($categories);
                $book->authors()->sync($authors);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Returns book by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $book = Book::with(['publisher', 'categories', 'authors'])
            ->find($id);

        if (is_null($book)) {
            return response()->json(null, 404);
        }

        return $book;
    }

    /**
     * Updates book by its ID.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::find($id);

        if (is_null($book)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'isbn' => 'required|unique:books,isbn,' . $id,
                'title' => 'required',
                'qty' => 'required',
                'available' => 'required',
                'publisher_id' => 'required',
                'categories' => 'required|array',
                'authors' => 'required|array',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except(['categories', 'authors']);
            $categories = $request->get('categories');
            $authors = $request->get('authors');

            DB::transaction(function () use (
                $book,
                $user_data,
                $categories,
                $authors
            ) {
                $book->update($user_data);
                $book->categories()->sync($categories);
                $book->authors()->sync($authors);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes book by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);

        if (is_null($book)) {
            return response()->json(null, 404);
        }

        $book->delete();

        return response()->json();
    }
}
