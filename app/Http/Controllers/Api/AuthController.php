<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Authenticates user and returns JWT.
     *
     * @param Request $request
     * @return Illuminate\Http\Response;
     */
    public function authenticate(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'password' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        if (Auth::attempt($request->all())) {
            $jwt = Auth::user()->getJwt();

            return response()->json($jwt);
        }

        return response()->json('wrong_credentials', 422);
    }
}
