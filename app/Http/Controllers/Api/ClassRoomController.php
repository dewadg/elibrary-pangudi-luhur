<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\ClassRoom;

class ClassRoomController extends Controller
{
    /**
     * Returns available class rooms.
     *
     * @return Illuminate\Http\Response;
     */
    public function index()
    {
        return ClassRoom::get();
    }

    /**
     * Stores new class room.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'grade' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        ClassRoom::create($request->all());

        return response()->json(null, 201);
    }

    /**
     * Returns class room by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $class_room = ClassRoom::find($id);

        if (is_null($class_room)) {
            return response()->json(null, 404);
        }

        return $class_room;
    }

    /**
     * Updates class room by its ID.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $class_room = ClassRoom::find($id);

        if (is_null($class_room)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'grade' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $class_room->update($request->all());

        return response()->json();
    }

    /**
     * Deletes class room by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $class_room = ClassRoom::find($id);

        if (is_null($class_room)) {
            return response()->json(null, 404);
        }

        $class_room->delete();

        return response()->json();        
    }
}
