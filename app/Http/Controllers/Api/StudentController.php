<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Student;

class StudentController extends Controller
{
    /**
     * Returns available students.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Student::with('classRoom')
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Stores new student.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'id' => 'required|unique:students,id',
                'student_number' => 'sometimes|unique:students,student_number',
                'name' => 'required',
                'gender' => 'required',
                'class_room_id' => 'required',
                'date_of_birth' => 'required|date',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        Student::create($request->all());

        return response()->json(null, 201);
    }

    /**
     * Returns student by its ID.
     *
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $student = Student::with('classRoom')
            ->find($id);

        if (is_null($student)) {
            return response()->json(null, 404);
        }

        return $student;
    }

    /**
     * Updates student by its ID.
     *
     * @param Request $request
     * @param string $id
     * @return Illuminate\Http\Request
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);

        if (is_null($student)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'gender' => 'required',
                'class_room_id' => 'required',
                'date_of_birth' => 'required|date',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $student->update($request->all());

        return response()->json();
    }

    /**
     * Deletes student by its ID
     *
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);

        if (is_null($student)) {
            return response()->json(null, 404);
        }

        $student->delete();

        return response()->json();
    }

    /**
     * Checks whether ID is unique or not.
     *
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function checkId($id)
    {
        $student = Student::find($id);

        if (is_null($student)) {
            return response()->json(true);
        }

        return response()->json(false);
    }
}
