<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ReportService;

class ReportController extends Controller
{
    /**
     * Stores the service instance.
     *
     * @var ReportService
     */
    protected $service;

    public function __construct()
    {
        $this->service = new ReportService;
    }

    /**
     * Returns broken books.
     *
     * @return Illuminate\Http\Response
     */
    public function brokenBooks()
    {
        return $this->service->brokenBooks();
    }

    /**
     * Groups transactions per student.
     *
     * @param mixed $year
     * @param mixed $month
     * @return Illuminate\Http\Response
     */
    public function transactions($year, $month)
    {
        return $this->service->transactions($year, $month);
    }
}
