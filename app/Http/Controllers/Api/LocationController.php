<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Location;

class LocationController extends Controller
{
    /**
     * Returns available books.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Location::with('books')
            ->get();
    }

    /**
     * Stores new book.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        Location::create($request->all());

        return response()->json(null, 201);
    }

    /**
     * Returns location by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $location = Location::with('books')
            ->find($id);

        if (is_null($location)) {
            return response()->json($validation->errors(), 422);
        }

        return $location;
    }

    /**
     * Updates location by its ID.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location = Location::with('books')
            ->find($id);

        if (is_null($location)) {
            return response()->json($validation->errors(), 422);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $location->update($request->all());

        return response()->json();
    }

    /**
     * Deletes location by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::with('books')
            ->find($id);

        if (is_null($location)) {
            return response()->json($validation->errors(), 422);
        }

        $location->delete();

        return response()->json();
    }
}
