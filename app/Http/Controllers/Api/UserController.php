<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Returns available users.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return User::get();
    }

    /**
     * Stores a new user.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:users,name',
                'full_name' => 'required',
                'password' => 'required',
                'password_conf' => 'required|same:password',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $user_data['password'] = bcrypt($user_data['password']);

        User::create($user_data);

        return response()->json(201);
    }

    /**
     * Returns user by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json(null, 404);
        }

        return $user;
    }

    /**
     * Updates user by its ID.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:users,name,' . $id,
                'full_name' => 'required',
                'password' => 'sometimes|required',
                'password_conf' => 'required_with:password|same:password',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors());
        }

        $user_data = $request->all();

        if (isset($user_data['password'])) {
            $user_data['password'] = bcrypt($user_data['password']);
        }

        $user->update($user_data);

        return response()->json();
    }

    /**
     * Deletes a user.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json(null, 404);
        }

        $user->delete();

        return response()->json();
    }

    /**
     * Returns current user.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function WhoAmI(Request $request)
    {
        return $request->user;
    }
}
