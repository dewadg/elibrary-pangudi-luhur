<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Returns available categories.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Category::get();
    }

    /**
     * Stores new category.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            ['name' => 'required']
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        Category::create($request->all());

        return response()->json(null, 201);
    }

    /**
     * Returns category by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $category = Category::find($id);

        if (is_null($category)) {
            return response()->json(null, 404);
        }

        return $category;
    }

    /**
     * Updates category by its ID.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        if (is_null($category)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            ['name' => 'required']
        );

        if ($validation->fails()) {
            return response()->json($validation->errors());
        }

        $category->update($request->all());

        return response()->json();
    }

    /**
     * Deletes category by its ID.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if (is_null($category)) {
            return response()->json(null, 404);
        }

        $category->delete();

        return response()->json();
    }
}
