<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Picqer\Barcode\BarcodeGeneratorPNG;
use App\Services\ReportService;
use App\ClassRoom;
use Carbon\Carbon;
use PDF;

class ExportController extends Controller
{
    /**
     * Stores the instance service.
     *
     * @var ReportService
     */
    protected $service;

    public function __construct()
    {
        $this->service = new ReportService;
    }

    /**
     * Exports students data by class room.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function classRoom($id)
    {
        $barcode_generator = new BarcodeGeneratorPNG;

        $students = ClassRoom::with(['students', 'students.classRoom'])
            ->find($id)
            ->students
            ->map(function ($item) use ($barcode_generator) {
                $barcode = base64_encode($barcode_generator->getBarcode($item->id, $barcode_generator::TYPE_CODE_128));

                $item->barcode_string = 'data:image/png;base64,' . $barcode;

                return $item;
            });

        $pdf = PDF::loadView('pdf.class-rooms', compact('students'));

        return $pdf->stream();
    }

    /**
     * Returns broken books.
     *
     * @return Illuminate\Http\Response
     */
    public function brokenBooks()
    {
        $total = 0;

        $books = $this->service->brokenBooks()
            ->map(function ($item) use (&$total) {
                $categories = $item->categories
                    ->map(function ($item) {
                        return $item->name;
                    })
                    ->all();

                $item->categories = implode(', ', $categories);
                $total += $item->broken;

                return $item;
            });

        $pdf = PDF::loadView('pdf.reports.broken-books', compact('total', 'books'));

        return $pdf->stream();
    }

    /**
     * Groups transactions per student.
     *
     * @param mixed $year
     * @param mixed $month
     * @return Illuminate\Http\Response
     */
    public function transactions($year, $month)
    {
        $total = 0;
        $date = Carbon::createFromFormat('Y-m', $year . '-' . $month)->format('F Y');
        $title = 'Laporan Peminjaman ' . $date;

        $students = $this->service->transactions($year, $month)
            ->each(function ($item) use (&$total) {
                $total += $item['total'];
            })
            ->all();

        $pdf = PDF::loadView('pdf.reports.transactions', compact('title', 'total', 'students'));

        return $pdf->stream();
    }

    /**
     * Groups transactions per class.
     *
     * @param mixed $year
     * @param mixed $month
     * @return array
     */
    public function transactionsPerClass($year, $month)
    {
        $date = Carbon::createFromFormat('Y-m', $year . '-' . $month)->format('m/Y');
        $title = 'Laporan Peminjaman Bulan ' . $date;
        $class_rooms = ClassRoom::get();
        $data = $this->service->transactionsPerClass($year, $month);
        $pdf = PDF::loadView('pdf.reports.transactions-per-class', compact('title', 'class_rooms', 'data'));

        return $pdf->stream();
    }
}
