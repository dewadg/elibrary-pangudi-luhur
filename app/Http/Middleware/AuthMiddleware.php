<?php

namespace App\Http\Middleware;

use Closure;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Carbon\Carbon;
use App\AuthToken;
use App\User;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @throws \Exception
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->header('Authorization'))) {
            return response()->json('token_required', 400);
        }

        try {
            $token_string = explode(' ', $request->header('Authorization'))[1];
            $decoded_token = $this->validateToken($token_string);
            $token = AuthToken::where('token', $token_string)->first();

            if (is_null($token)) {
                throw new \Exception('invalid_auth_token');
            }

            if (! is_null($token->last_hit)) {
                $now = Carbon::now();
                $last_hit = $token->last_hit->addMinutes(60);

                if ($now->gt($last_hit)) {
                    throw new \Exception('expired_auth_token');
                }
            }

            $token->update(['last_hit' => Carbon::now()]);
            $request->user = User::find($decoded_token->getClaim('user_id'));

            if (is_null($request->user)) {
                throw \Exception('user_not_found');
            }

            return $next($request);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    /**
     * Validates the JWT.
     *
     * @param string $token
     * @return string
     * @throws \Exception
     */
    public function validateToken($token)
    {
        $token = (new Parser)->parse($token);
        $signer = new Sha256;

        if (!$token->verify($signer, env('JWT_SIGN_KEY'))) {
            throw new \Exception('invalid_auth_token');
        }

        return $token;
    }
}
