<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Book;
use App\Transaction;
use App\ClassRoom;
use Carbon\Carbon;

class ReportService
{
    /**
     * Returns broken books.
     *
     * @return array
     */
    public function brokenBooks()
    {
        return Book::with(['publisher', 'categories'])
            ->whereRaw('broken > 0')
            ->get();
    }

    /**
     * Groups transactions per student.
     *
     * @param mixed $year
     * @param mixed $month
     * @return array
     */
    public function transactions($year, $month)
    {
        return Transaction::with(['student', 'student.classRoom'])
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->get()
            ->groupBy(function ($item) {
                return $item->student->id;
            })
            ->map(function ($item, $key) {
                return [
                    'id' => $key,
                    'name' => $item[0]->student->name,
                    'total' => $item->count(),
                ];
            })
            ->sortByDesc(function ($item) {
                return $item['total'];
            })
            ->values();
    }

    /**
     * Groups transactions per class.
     *
     * @param mixed $year
     * @param mixed $month
     * @return array
     */
    public function transactionsPerClass($year, $month)
    {
        $data = [];
        $end = Carbon::createFromFormat('Y-m', $year . '-' . $month)->endOfMonth();
        $class_rooms = ClassRoom::get();

        for (
            $now = Carbon::createFromFormat('Y-m', $year . '-' . $month)->startOfMonth();
            $now->lte($end);
            $now->addDay(1)
        ) {
            $class_rooms->each(function ($item) use ($now, &$data) {
                $data[$now->format('d')][$item->id] = [
                    'name' => $item->name,
                    'count' => 0,
                ];
            });

            $data[$now->format('d')]['total'] = ['count' => 0];
        }

        Transaction::with(['student', 'student.classRoom'])
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->get()
            ->groupBy(function ($item) {
                return $item->created_at->format('d');
            })
            ->each(function ($item, $key) use ($class_rooms, &$data) {
                $total = 0;

                $class_rooms->each(function ($class_room) use ($item, $key, &$data, &$total) {
                    $count = $item
                        ->filter(function ($item) use ($class_room) {
                            return $item->student->classRoom->id == $class_room->id;
                        })
                        ->count();

                    $total += $count;
                    $data[$key][$class_room->id]['count'] = $count;
                });

                $data[$key]['total']['count'] = $total;
            });

        return $data;
    }
}
