<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'student_number',
        'name',
        'gender',
        'class_room_id',
        'date_of_birth',
    ];

    /**
     * Set primary key to non incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Date-casted attributes.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'date_of_birth',
    ];

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
    ];

    protected $hidden = [
        'class_room_id',
    ];

    /**
     * Forms relationship to ClassRoom.
     *
     * @return ClassRoom
     */
    public function classRoom()
    {
        return $this->belongsTo(ClassRoom::class, 'class_room_id');
    }

    /**
     * Forms relationship to Transaction.
     *
     * @return array
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
