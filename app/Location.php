<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];

    /**
     * Forms relationship to Book.
     *
     * @return array
     */
    public function books()
    {
        return $this->belongsToMany(Book::class, 'book_location_map')
            ->withPivot(['qty']);
    }
}
