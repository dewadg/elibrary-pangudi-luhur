<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookClassification extends Model
{
    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];
}
