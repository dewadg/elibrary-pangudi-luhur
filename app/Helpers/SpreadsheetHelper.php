<?php

namespace App\Helpers;

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class SpreadsheetHelper
{
    /**
     * Stores the file location.
     *
     * @var string
     */
    protected $file;

    /**
     * Stores the Xlsx reader instance.
     *
     * @var PhpOffice\PhpSpreadsheet\Reader\Xlsx
     */
    protected $reader;

    /**
     * Stores
     *
     * @var PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    protected $spreadsheet;

    /**
     * Constructs the helper.
     */
    public function __construct()
    {
        $this->reader = new Xlsx;
        $this->reader->setReadDataOnly(true);
    }

    /**
     * Loads file from a location.
     *
     * @param string $file
     * @return void
     */
    public function loadFile($file)
    {
        $this->file = $file;
        $this->spreadsheet = $this->reader->load($this->file);
    }

    /**
     * Returns data within the spreadsheet.
     *
     * @return array
     */
    public function getData()
    {
        $worksheet = $this->spreadsheet->getActiveSheet();
        $highest_row = $worksheet->getHighestRow();
        $highest_column = $worksheet->getHighestColumn();

        return $worksheet->rangeToArray(
            'A7:' . $highest_column . $highest_row,
            null,
            true,
            true,
            false
        );
    }
}
