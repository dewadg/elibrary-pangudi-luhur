<?php

namespace App\Helpers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use App\Student;
use App\ClassRoom;

class StudentDataHelper
{
    /**
     * Stores the SpreadsheetHelper instance.
     *
     * @var App\Helpers\SpreadsheetHelper
     */
    protected $spreadsheet_helper;

    /**
     * Stores imported students data.
     *
     * @var Illuminate\Support\Collection
     */
    protected $data;

    /**
     * Stores the logger instance.
     *
     * @var Monolog\Logger
     */
    protected $logger;

    /**
     * Constructs the helper.
     */
    public function __construct()
    {
        $this->spreadsheet_helper = new SpreadsheetHelper;
    }

    /**
     * Loads data from file.
     *
     * @param string $file
     * @return void
     */
    public function loadFile($file)
    {
        $this->spreadsheet_helper->loadFile($file);
        $this->data = new Collection($this->spreadsheet_helper->getData());
        $this->logger = new Logger('students_import');
        $this->logger->pushHandler(new StreamHandler(storage_path('logs/students_import.log')), Logger::ERROR);
    }

    private function validate($data)
    {
        $validation = Validator::make(
            $data,
            [
                'id' => 'required|unique:students,id',
                'student_number' => 'required|unique:students,student_number',
                'name' => 'required',
                'gender' => 'required',
                'date_of_birth' => 'required|date',
                'class_room_id' => 'required',
            ]
        );

        return $validation->passes();
    }

    /**
     * Imports data to DB.
     *
     * @return void
     */
    public function import()
    {
        $data = $this->data;
        $class_rooms = [];
        
        ClassRoom::get()
            ->each(function ($item) use (&$class_rooms) {
                $class_rooms[$item->name] = $item->id;
            });

        try {
            $successfull_counts = 0;
            $failed_counts = 0;

            DB::transaction(function () use (
                $data,
                $class_rooms,
                &$successfull_counts,
                &$failed_counts
            ) {
                $data->each(function ($item) use (
                    $class_rooms,
                    &$successfull_counts,
                    &$failed_counts
                ) {
                    if (! is_null(Student::find($item[4]))) {
                        return;
                    }

                    $student_data = [];

                    try {
                        $student_data = [
                            'id' => $item[4],
                            'student_number' => $item[2],
                            'name' => ucwords(strtolower($item[1])),
                            'gender' => $item[3],
                            'date_of_birth' => $item[6],
                            'class_room_id' => $class_rooms[$item[42]],
                        ];
                    } catch (\Exception $e) {
                        $failed_counts += 1;
                        $this->logger->info(
                            'FAILED_IMPORT',
                            [
                                'NISN' => $item[4],
                                'No. Induk' => $item[2]
                            ]
                        );

                        return;
                    }

                    if (! $this->validate($student_data)) {
                        $failed_counts += 1;
                        $this->logger->info(
                            'FAILED_IMPORT',
                            [
                                'NISN' => $item[4],
                                'No. Induk' => $item[2]
                            ]
                        );

                        return;
                    }
                    
                    Student::create($student_data);
                    $successfull_counts += 1;
                });
            });

            return [
                'successfull' => $successfull_counts,
                'failed' => $failed_counts,
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
