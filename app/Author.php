<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes;
    
    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];

    /**
     * Forms relationship to Book.
     *
     * @return array
     */
    public function books()
    {
        return $this->belongsToMany(Book::class, 'author_book_map');
    }
}
