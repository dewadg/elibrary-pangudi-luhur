<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'full_name',
        'password',
    ];

    /**
     * Date-casted attributes.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJwt()
    {
        $signer = new Sha256;
        $token = (new Builder)
            ->setIssuer(env('APP_URL'))
            ->setAudience((env('APP_URL')))
            ->setIssuedAt(time())
            ->set('user_id', $this->id)
            ->sign($signer, env('JWT_SIGN_KEY'))
            ->getToken();

        AuthToken::create(['token' => (string) $token]);

        return (string) $token;
    }
}
