<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id',
        'book_id',
        'qty',
    ];

    /**
     * Hidden attributes.
     *
     * @var array
     */
    protected $hidden = [
        'transaction_id',
        'book_id',
    ];

    /**
     * Disables timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Forms relationship to Book.
     *
     * @return Book
     */
    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
