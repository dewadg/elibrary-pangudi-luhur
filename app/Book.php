<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    /**
     * Atrributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'isbn',
        'title',
        'date_of_publication',
        'qty',
        'available',
        'in_rent',
        'broken',
        'publisher_id',
        'book_classification_id',
    ];

    /**
     * Date-casted attributes.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Hidden attributes.
     *
     * @var array
     */
    protected $hidden = [
        'publisher_id',
        'book_classification_id',
    ];

    /**
     * Forms relationship to Category.
     *
     * @return array
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'book_category_map');
    }

    /**
     * Forms relationship to Location.
     *
     * @return array
     */
    public function locations()
    {
        return $this->belongsToMany(Location::class, 'book_location_map')
            ->withPivot(['qty']);
    }

    /**
     * Forms relationship to Publisher.
     *
     * @return Publisher
     */
    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    /**
     * Forms relationship to Author.
     *
     * @return array
     */
    public function authors()
    {
        return $this->belongsToMany(Author::class, 'author_book_map');
    }

    /**
     * Forms relationship to BookClassification
     *
     * @return BookClassification
     */
    public function classification()
    {
        return $this->belongsTo(BookClassification::class, 'book_classification_id');
    }
}
