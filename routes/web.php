<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('export/student-cards/{id}', 'ExportController@classRoom');
Route::get('export/broken-books', 'ExportController@brokenBooks');
Route::get('export/transactions/{year}/{month}', 'ExportController@transactions');
Route::get('export/transactions-per-class/{year}/{month}', 'ExportController@transactionsPerClass');