<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$config = [
    'middleware' => ['api', 'auth'],
    'namespace' => 'Api',
];

Route::post('auth', 'Api\AuthController@authenticate');

Route::group($config, function () {
    // User
    Route::get('users', 'UserController@index');
    Route::post('users', 'UserController@store');
    Route::get('users/{id}', 'UserController@get');
    Route::patch('users/{id}', 'UserController@update');
    Route::delete('users/{id}', 'UserController@destroy');
    Route::get('whoami', 'UserController@whoami');

    // Category
    Route::get('categories', 'CategoryController@index');
    Route::post('categories', 'CategoryController@store');
    Route::get('categories/{id}', 'CategoryController@get');
    Route::patch('categories/{id}', 'CategoryController@update');
    Route::delete('categories/{id}', 'CategoryController@destroy');

    // ClassRoom
    Route::get('class-rooms', 'ClassRoomController@index');
    Route::post('class-rooms', 'ClassRoomController@store');
    Route::get('class-rooms/{id}', 'ClassRoomController@get');
    Route::patch('class-rooms/{id}', 'ClassRoomController@update');
    Route::delete('class-rooms/{id}', 'ClassRoomController@delete');

    // Student
    Route::get('students', 'StudentController@index');
    Route::post('students', 'StudentController@store');
    Route::get('students/check-id/{id}', 'StudentController@checkId');
    Route::get('students/{id}', 'StudentController@get');
    Route::patch('students/{id}', 'StudentController@update');
    Route::delete('students/{id}', 'StudentController@destroy');

    // Location
    Route::get('locations', 'LocationController@index');
    Route::post('locations', 'LocationController@store');
    Route::get('locations/{id}', 'LocationController@get');
    Route::patch('locations/{id}', 'LocationController@update');
    Route::delete('locations/{id}', 'LocationController@destroy');

    // Book
    Route::get('books', 'BookController@index');
    Route::post('books', 'BookController@store');
    Route::get('books/{id}', 'BookController@get');
    Route::patch('books/{id}', 'BookController@update');
    Route::delete('books/{id}', 'BookController@destroy');

    // Author
    Route::get('authors', 'AuthorController@index');
    Route::post('authors', 'AuthorController@store');
    Route::get('authors/{id}', 'AuthorController@get');
    Route::patch('authors/{id}', 'AuthorController@update');
    Route::delete('authors/{id}', 'AuthorController@destroy');

    // Publisher
    Route::get('publishers', 'PublisherController@index');
    Route::post('publishers', 'PublisherController@store');
    Route::get('publishers/{id}', 'PublisherController@get');
    Route::patch('publishers/{id}', 'PublisherController@update');
    Route::delete('publishers/{id}', 'PublisherController@destroy');

    // Transaction
    Route::get('transactions', 'TransactionController@index');
    Route::post('transactions/rent', 'TransactionController@rent');
    Route::get('transactions/{id}', 'TransactionController@get');
    Route::post('transactions/{id}/return', 'TransactionController@return');
    Route::delete('transactions/{id}', 'TransactionController@destroy');

    // Stats
    Route::get('stats/basic', 'StatsController@basic');
    Route::get('stats/class-rooms', 'StatsController@classRooms');
    Route::get('stats/book-categories', 'StatsController@bookCategories');

    // Reports
    Route::get('reports/broken-books', 'ReportController@brokenBooks');
    Route::get('reports/transactions/{year}/{month}', 'ReportController@transactions');
});
